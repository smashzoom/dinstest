package com.somedomain;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.MalformedURLException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;

import com.tngtech.java.junit.dataprovider.DataProviderRunner;

import static org.junit.Assert.assertEquals;

@RunWith(DataProviderRunner.class)
public class TestClassDP {
    private HttpsURLConnection connection;

    @DataProvider
    public static Object[][] dataProviderAdd() {
        return new Object[][]{
                {"NOTEXIST", "Exister", HttpsURLConnection.HTTP_NOT_FOUND},
                {"", "Exister", HttpsURLConnection.HTTP_NOT_FOUND},
                {"-1”!»№;%:?*()Э", "Exister", HttpsURLConnection.HTTP_NOT_FOUND},
                {"EXIST", "NotExister", HttpsURLConnection.HTTP_NO_CONTENT},
                {"EXIST", "", HttpsURLConnection.HTTP_NO_CONTENT},
                {"EXIST", "-1”!»№;%:?*()Э", HttpsURLConnection.HTTP_NO_CONTENT},
                {"EXIST", "Exister", HttpsURLConnection.HTTP_OK},
        };
    }

    @Test
    @UseDataProvider("dataProviderAdd")
    public void test(String companyName, String userName, int responseCode) throws IOException {
        connection = (HttpsURLConnection) setUrl(companyName, userName).openConnection(); //Задаем нужный нам URL и открываем соединение
        assertEquals(responseCode, connection.getResponseCode()); //Проверяем код статуса
    }

    public URL setUrl(String companyName, String userName) {
        try {
            URL url = new URL("https://some_domain.com/company/" + companyName + "/users?name=" + userName);
            return url;
        } catch (MalformedURLException e) {
            System.out.println("The specified URL is malformed: " + e.getMessage());
        }
        return null;
    }
}

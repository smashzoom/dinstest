package com.somedomain;

import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class TestClass {
    private HttpsURLConnection connection;
    private String[] companies = {"EXIST", "NOTEXIST", "", "-1”!»№;%:?*()Э"};
    private String[] users = {"Exister", "NotExister", "", "-1”!»№;%:?*()Э"};

    @Test
    public void testOne() throws IOException {
        connection = (HttpsURLConnection) setUrl(companies[1], users[0]).openConnection(); //Задаем нужный нам URL и открываем соединение
        assertEquals(HttpsURLConnection.HTTP_NOT_FOUND, connection.getResponseCode()); //Проверяем код статуса
    }

    @Test
    public void testTwo() throws IOException {
        connection = (HttpsURLConnection) setUrl(companies[2], users[0]).openConnection(); //Задаем нужный нам URL и открываем соединение
        assertEquals(HttpsURLConnection.HTTP_NOT_FOUND, connection.getResponseCode()); //Проверяем код статуса
    }

    @Test
    public void testThree() throws IOException {
        connection = (HttpsURLConnection) setUrl(companies[3], users[0]).openConnection(); //Задаем нужный нам URL и открываем соединение
        assertEquals(HttpsURLConnection.HTTP_NOT_FOUND, connection.getResponseCode()); //Проверяем код статуса
    }

    @Test
    public void testFour() throws IOException {
        connection = (HttpsURLConnection) setUrl(companies[0], users[1]).openConnection(); //Задаем нужный нам URL и открываем соединение
        assertEquals(HttpsURLConnection.HTTP_NO_CONTENT, connection.getResponseCode()); //Проверяем код статуса
    }

    @Test
    public void testFive() throws IOException {
        connection = (HttpsURLConnection) setUrl(companies[0], users[2]).openConnection(); //Задаем нужный нам URL и открываем соединение
        assertEquals(HttpsURLConnection.HTTP_NO_CONTENT, connection.getResponseCode()); //Проверяем код статуса
    }

    @Test
    public void testSix() throws IOException {
        connection = (HttpsURLConnection) setUrl(companies[0], users[3]).openConnection(); //Задаем нужный нам URL и открываем соединение
        assertEquals(HttpsURLConnection.HTTP_NO_CONTENT, connection.getResponseCode()); //Проверяем код статуса
    }

    @Test
    public void testSeven() throws IOException {
        connection = (HttpsURLConnection) setUrl(companies[0], users[0]).openConnection(); //Задаем нужный нам URL и открываем соединение
        assertEquals(HttpsURLConnection.HTTP_OK, connection.getResponseCode()); //Проверяем код статуса

    }

    private URL setUrl(String companyName, String userName) {
        try {
            URL url = new URL("https://some_domain.com/company/" + companyName + "/users?name=" + userName);
            return url;
        } catch (MalformedURLException e) {
            System.out.println("The specified URL is malformed: " + e.getMessage());
        }
        return null;
    }
}
